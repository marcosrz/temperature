import axiosHttpClient from '../axios/axios-temp'

export const getTemperature = () => {
  
  return axiosHttpClient.get('/temperature');

}

export default { getTemperature };