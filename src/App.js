import React, { useState, useEffect } from 'react';
import './App.css';
import { getTemperature } from './services/temperature.service';
import Temperature from './Temperature';
import Loading from './Loading';
import Error from './Error';

const App = () => {

  const [temperature, setTemperature ] = useState(0);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {

    setLoading(true);

    getTemperature().then(res => {
      console.log(res);
      setTemperature(res.data.temperature)
      setLoading(false);
    }).catch(err => {
      setError(true);
      setLoading(false);
    });

  }, []);

  return <div className="App">
      <header className="App-header">
        { loading 
          ? <Loading />
          : <div className="content">
            { !error 
              ? <Temperature temperature={temperature} /> 
              : <Error />
            }
          </div>
        }
      </header>
    </div>
  ;
}

export default App;
