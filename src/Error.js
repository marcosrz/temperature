import React from 'react';

const Error = () => {
  return (
    <div>
      <h2>Algo foi mal</h2>
      <span className="quote">🥺</span> 
      <p className="description">Recarga pra volver a intentallo!</p>
    </div>
  );
};

export default Error;