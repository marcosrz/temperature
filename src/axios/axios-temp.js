import axios from 'axios';

const instance = axios.create({
  // baseURL: 'http://raspberrypi.local:3001',
  baseURL: 'https://temp-api.servebeer.com',
  // timeout: 1000,
  // headers: {'X-Custom-Header': 'foobar'}
});

export default instance;