import React from 'react';

const colors = {
  low: {
    color: '#02d5ff',
    description: 'Fai frío abondo. A lo meyor hai qu\'ir pensando en abrir esa botellía de vodka...',
    emoji: '❄'
  },
  ok: {
    color: '#00FF00',
    description: 'Táse ben. Botella de alvariño y úas aceitunías',
    emoji: '😊'
  },
  mid: {
    color: '#FF9900',
    description: 'Fai algo de calor pero inda nun se ta queimando nada. Mira a ver si hai estrellas na nevera...',
    emoji: '🔥'
  },
  hi: {
    color: '#FF0000',
    description: 'Mira a ver si se ta queimando algo !!!',
    emoji: '🌋'
  },
};

const Temperature = (props) => {

  const { temperature } = props;

  let color = '#DDD';

  if (temperature <= 15) {
    console.log(1);
    color = colors.low;
  } else if (temperature <= 23) {
    console.log(2);
    color = colors.ok;
  } else if (temperature < 30) {
    console.log(3);
    color = colors.mid;
  } else {
    console.log(4);
    color = colors.hi;
  }

  return (
    <div>
      <h1 style={{ color: color.color }}>{props.temperature} ºC</h1>
      <span className="quote">{color.emoji}</span> 
      <p className="description">{ color.description }</p>
    </div>
  );
};

export default Temperature;