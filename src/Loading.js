import React from 'react';
import loading from './loading.svg';

const Loading = () => {
  return (
    <div className="content">
      <p className="loading">Reading sensors</p>
      <img src={loading} alt="loading sprite" />
    </div>
  );
};

export default Loading;